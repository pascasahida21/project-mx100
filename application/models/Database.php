<?php

class Database extends CI_Model
{
    public function get_data($table, $where_clause = null)
    {
        if ($where_clause === null) {
            return $this->db->get($table);
        } else {
            $this->db->where($where_clause);
            return $this->db->get($table);
        }
    }

    public function get_data_full_JobOffer()
    {
        $this->db->select('*');
        $this->db->from('job_offer jo');
        $this->db->join('company c', 'jo.jof_CompanyId = c.com_Id');
        $this->db->where('jo.jof_Status', 'Publish');
        return $this->db->get();
    }

    public function get_data_full_JobApply($id = null)
    {
        if ($id === null) {
            $this->db->select('*');
            $this->db->from('job_apply ja');
            $this->db->join('job_offer jo', 'ja.ja_JobOfferId = jo.jof_Id');
            $this->db->join('jobseeker js', 'ja.ja_JobseekerId = js.js_Id');
            $this->db->join('company c', 'jo.jof_CompanyId = c.com_Id');
            return $this->db->get();
        }else {
            $this->db->select('*');
            $this->db->from('job_apply ja');
            $this->db->join('job_offer jo', 'ja.ja_JobOfferId = jo.jof_Id');
            $this->db->join('jobseeker js', 'ja.ja_JobseekerId = js.js_Id');
            $this->db->join('company c', 'jo.jof_CompanyId = c.com_Id');
            $this->db->where('ja.ja_Id', $id);
            return $this->db->get();
        }

    }

    public function add_data($table, $data)
    {
        return $this->db->insert($table, $data);
    }

    public function update_data($table, $data, $where_clause)
    {
        $this->db->where($where_clause);
        return $this->db->update($table, $data);
    }
}
