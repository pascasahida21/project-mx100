<?php

use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Job_offer extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Database');
        date_default_timezone_set('Asia/Jakarta');
    }

    public function index_get()
    {
        $data_JobOffer_Formated = array();
        $id = $this->get('id');

        if ($id === null) {
            $data_JobOffer = $this->Database->get_data('job_offer');
        } else {
            $data_JobOffer = $this->Database->get_data('job_offer', array('jof_Id' => $id));
        }

        if ($data_JobOffer->num_rows() != 0) {

            foreach ($data_JobOffer->result() as $dj) {
                array_push(
                    $data_JobOffer_Formated,
                    [
                        "jof_Id" => $dj->jof_Id,
                        "jof_CompanyId" => $dj->jof_CompanyId,
                        "jof_Tittle" => $dj->jof_Tittle,
                        "jof_Description" => $dj->jof_Description,
                        "jof_Status" => $dj->jof_Status,
                        "last_update" => date("Y-m-d H:i:s", strtotime($dj->last_update . ' UTC'))
                    ]
                );
            }

            $this->response([
                "status" => true,
                "message" => 'Berhasil mengambil data penawaran pekerjaan',
                "data" => $data_JobOffer_Formated
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                "status" => false,
                "message" => 'Id data penawaran pekerjaan tidak ditemukan'
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }


    public function forJobseeker_get()
    {
        $data_JobOffer_Formated = array();

        $data_JobOffer = $this->Database->get_data_full_JobOffer();

        if ($data_JobOffer->num_rows() != 0) {

            foreach ($data_JobOffer->result() as $dj) {
                array_push(
                    $data_JobOffer_Formated,
                    [
                        "jof_Id" => $dj->jof_Id,
                        "jof_CompanyId" => $dj->jof_CompanyId,
                        "jof_CompanyName" => $dj->com_Name,
                        "jof_Tittle" => $dj->jof_Tittle,
                        "jof_Description" => $dj->jof_Description,
                        "jof_Status" => $dj->jof_Status,
                        "last_update" => date("Y-m-d H:i:s", strtotime($dj->last_update . ' UTC'))
                    ]
                );
            }

            $this->response([
                "status" => true,
                "message" => 'Berhasil mengambil data penawaran pekerjaan untuk Jobseeker',
                "data" => $data_JobOffer_Formated
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                "status" => false,
                "message" => 'Gagal mengambil data penawaran pekerjaan untuk Jobseeker'
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }


    public function index_post()
    {
        $data = [
            'jof_CompanyId' => $this->post('CompanyId'),
            'jof_Tittle' => $this->post('Tittle'),
            'jof_Description' => $this->post('Description'),
            'jof_Status' => $this->post('Status'),
            'last_update' => gmdate("Y-m-d h:i:s")
        ];

        if ($this->Database->add_data('job_offer', $data) > 0) {
            $this->response([
                "status" => true,
                "message" => 'Berhasil menambah penawaran pekerjaan'
            ], REST_Controller::HTTP_CREATED);
        } else {
            $this->response([
                "status" => false,
                "message" => 'Gagal menambah penawaran pekerjaan'
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }


    public function index_put()
    {
        $id = $this->put('id');
        $data = [
            'jof_CompanyId' => $this->put('CompanyId'),
            'jof_Tittle' => $this->put('Tittle'),
            'jof_Description' => $this->put('Description'),
            'jof_Status' => $this->put('Status'),
            'last_update' => gmdate("Y-m-d h:i:s")
        ];

        if ($this->Database->update_data('job_offer', $data, array('jof_Id' => $id))) {
            $this->response([
                "status" => true,
                "message" => 'Berhasil memperbarui penawaran pekerjaan'
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                "status" => false,
                "message" => 'Gagal memperbarui penawaran pekerjaan'
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }
}
