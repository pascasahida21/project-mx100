<?php

use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Job_apply extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Database');
        date_default_timezone_set('Asia/Jakarta');
    }

    public function index_get()
    {
        $data_JobApply_Formated = array();
        $id = $this->get('id');

        if ($id === null) {
            $data_JobApply = $this->Database->get_data_full_JobApply();
        } else {
            $data_JobApply = $this->Database->get_data_full_JobApply($id);
        }

        if ($data_JobApply->num_rows() != 0) {

            foreach ($data_JobApply->result() as $dj) {
                array_push(
                    $data_JobApply_Formated,
                    [
                        "ja_Id" => $dj->ja_Id,
                        "ja_CompanyName" => $dj->com_Name,
                        "ja_JobOfferId" => $dj->ja_JobOfferId,
                        "ja_JobOfferTittle" => $dj->jof_Tittle,
                        "ja_JobseekerId" => $dj->ja_JobseekerId,
                        "ja_JobseekerName" => $dj->js_Name,
                        "ja_Cv" => base_url("assets/document-cv/") . $dj->ja_Cv,
                        "last_update" => date("Y-m-d H:i:s", strtotime($dj->last_update . ' UTC'))
                    ]
                );
            }

            $this->response([
                "status" => true,
                "message" => 'Berhasil mengambil data pelamar pekerjaan',
                "data" => $data_JobApply_Formated
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                "status" => false,
                "message" => 'Id data pelamar pekerjaan tidak ditemukan'
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }


    public function index_post()
    {
        $namefile = "file_" . time();
        $config['upload_path'] = './assets/document-cv/';
        $config['allowed_types'] = 'jpeg|jpg|png|pdf';
        $config['max_size'] = '4048';
        $config['file_name'] = $namefile;

        $check = $this->Database->get_data('job_apply', array('ja_JobOfferId' => $this->post('JobOfferId'), 'ja_JobseekerId' => $this->post('JobseekerId')));

        if ($check->num_rows() == 0) {
            $this->upload->initialize($config);
            if ($this->upload->do_upload('file')) {
                $file = $this->upload->data();

                $data = [
                    'ja_JobOfferId' => $this->post('JobOfferId'),
                    'ja_JobseekerId' => $this->post('JobseekerId'),
                    'ja_cv' => $file['file_name'],
                    'last_update' => gmdate("Y-m-d h:i:s")
                ];
                if ($this->Database->add_data('job_apply', $data) > 0) {
                    $this->response([
                        "status" => true,
                        "message" => 'Berhasil menambah lamaran pekerjaan'
                    ], REST_Controller::HTTP_CREATED);
                } else {
                    $this->response([
                        "status" => false,
                        "message" => 'Gagal menambah lamaran pekerjaan'
                    ], REST_Controller::HTTP_BAD_REQUEST);
                }
            } else {
                $this->response([
                    "status" => false,
                    "message" => $this->upload->display_errors()
                ], REST_Controller::HTTP_BAD_REQUEST);
            }
        } else {
            $this->response([
                "status" => false,
                "message" => 'Jobseeker sudah melamar pekerjaan ini'
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }
}
