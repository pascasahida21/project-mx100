-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Waktu pembuatan: 27 Agu 2020 pada 18.34
-- Versi server: 5.7.26
-- Versi PHP: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_mx100`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `company`
--

CREATE TABLE `company` (
  `com_Id` int(11) NOT NULL,
  `com_Name` varchar(100) NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `company`
--

INSERT INTO `company` (`com_Id`, `com_Name`, `last_update`) VALUES
(1, 'CV Kudaku', '2020-08-27 17:08:37'),
(2, 'PT ABC', '2020-08-27 17:08:22'),
(3, 'PT Mahesa', '2020-08-27 17:08:22'),
(4, 'PT Jaya Abadi', '2020-08-27 17:08:22'),
(5, 'PT Timbul Tenggelam', '2020-08-27 17:08:22'),
(6, 'PT Pengais Rejeki', '2020-08-27 17:08:22'),
(7, 'PT Maju Jaya', '2020-08-27 17:08:22'),
(8, 'PT Makmur Abadi', '2020-08-27 17:08:22'),
(9, 'PT Nahusam', '2020-08-27 17:08:22'),
(10, 'PT Otigade', '2020-08-27 17:08:22'),
(11, 'CV Tsar', '2020-08-27 17:08:22');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jobseeker`
--

CREATE TABLE `jobseeker` (
  `js_Id` int(11) NOT NULL,
  `js_Name` varchar(100) NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `jobseeker`
--

INSERT INTO `jobseeker` (`js_Id`, `js_Name`, `last_update`) VALUES
(1, 'Eko', '2020-08-27 17:25:47'),
(2, 'Intan', '2020-08-27 17:25:47'),
(3, 'Ratna', '2020-08-27 17:25:47'),
(4, 'Juwita', '2020-08-27 17:25:47'),
(5, 'Budi', '2020-08-27 17:25:47'),
(6, 'Hartono', '2020-08-27 17:25:47'),
(7, 'Mangica', '2020-08-27 17:25:47'),
(8, 'Agus', '2020-08-27 17:25:47'),
(9, 'Bambang', '2020-08-27 17:25:47'),
(10, 'Yuwono', '2020-08-27 17:25:47');

-- --------------------------------------------------------

--
-- Struktur dari tabel `job_apply`
--

CREATE TABLE `job_apply` (
  `ja_Id` int(11) NOT NULL,
  `ja_JobOfferId` int(11) NOT NULL,
  `ja_JobseekerId` int(11) NOT NULL,
  `ja_Cv` varchar(1000) NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `job_apply`
--

INSERT INTO `job_apply` (`ja_Id`, `ja_JobOfferId`, `ja_JobseekerId`, `ja_Cv`, `last_update`) VALUES
(1, 1, 2, 'file_1598532654.jpg', '2020-08-27 05:50:54'),
(2, 2, 2, 'file_1598532805.png', '2020-08-27 05:53:25'),
(3, 1, 3, 'file_1598534260.png', '2020-08-26 18:17:40'),
(4, 1, 5, 'file_1598542498.png', '2020-08-26 20:34:58');

-- --------------------------------------------------------

--
-- Struktur dari tabel `job_offer`
--

CREATE TABLE `job_offer` (
  `jof_Id` int(11) NOT NULL,
  `jof_CompanyId` int(11) NOT NULL,
  `jof_Tittle` varchar(100) NOT NULL,
  `jof_Description` text NOT NULL,
  `jof_Status` varchar(100) NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `job_offer`
--

INSERT INTO `job_offer` (`jof_Id`, `jof_CompanyId`, `jof_Tittle`, `jof_Description`, `jof_Status`, `last_update`) VALUES
(1, 1, 'freelance', 'apa aja', 'publish', '2020-08-27 00:59:18'),
(2, 2, 'freelance lagi', 'apa aja lagi', 'draft', '2020-08-27 09:53:06'),
(3, 3, 'freelance lagi', 'apa aja lagi', 'publish', '2020-08-27 01:06:20'),
(4, 4, 'freelance lagi aja deh', 'apa aja lagi4', 'publish', '2020-08-27 04:36:13'),
(5, 4, 'freelance lagi 4', 'apa aja lagi4', 'publish', '2020-08-27 04:16:04'),
(6, 4, 'freelance lagi 4', 'apa aja lagi4', 'publish', '2020-08-27 04:19:40'),
(7, 4, 'Frelance ', 'kerja dibayar', 'Publish', '2020-08-26 20:16:27');

-- --------------------------------------------------------

--
-- Struktur dari tabel `keys`
--

CREATE TABLE `keys` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `key` varchar(40) NOT NULL,
  `level` int(2) NOT NULL,
  `ignore_limits` tinyint(1) NOT NULL DEFAULT '0',
  `is_private_key` tinyint(1) NOT NULL DEFAULT '0',
  `ip_addresses` text,
  `date_created` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `keys`
--

INSERT INTO `keys` (`id`, `user_id`, `key`, `level`, `ignore_limits`, `is_private_key`, `ip_addresses`, `date_created`) VALUES
(1, 1, 'mpxuser1', 1, 0, 0, NULL, '2020-08-27 00:00:00.000000');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`com_Id`);

--
-- Indeks untuk tabel `jobseeker`
--
ALTER TABLE `jobseeker`
  ADD PRIMARY KEY (`js_Id`);

--
-- Indeks untuk tabel `job_apply`
--
ALTER TABLE `job_apply`
  ADD PRIMARY KEY (`ja_Id`);

--
-- Indeks untuk tabel `job_offer`
--
ALTER TABLE `job_offer`
  ADD PRIMARY KEY (`jof_Id`);

--
-- Indeks untuk tabel `keys`
--
ALTER TABLE `keys`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `company`
--
ALTER TABLE `company`
  MODIFY `com_Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `jobseeker`
--
ALTER TABLE `jobseeker`
  MODIFY `js_Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `job_apply`
--
ALTER TABLE `job_apply`
  MODIFY `ja_Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `job_offer`
--
ALTER TABLE `job_offer`
  MODIFY `jof_Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `keys`
--
ALTER TABLE `keys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
