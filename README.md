### **REST API Project Job Portal MX100 Menggunakan Codeigniter 3**
---

##### Author : Aulia Pasca Sahida 

#### **`Instalasi`**
1. Atur konfigurasi *base_url* pada file **config.php** (application/config), sesuaikan dengan host server.
2. Atur konfigurasi *database* pada file **database.php** (host,database,username,password), pastikan database telah tersedia, dan import file sql pada direktori *root* project ini.
#### **`API`**
`API Key` : key-mx100<br>  `Value` : mpxuser1 (Dinamis, bisa dilihat di tabel keys)<br> <br>
1. Perusahaan membuat dan memposting lowongan pekerjaan: <br/> *http://localhost:8888/project-mx100/api/job_offer/*  (`method post`)<br>
**Params** : 
- CompanyId
- Tittle
- Description
- Status<br><br>
2. Pencari kerja melihat lowongan pekerjaan: <br>
*http://localhost:8888/project-mx100/api/job_offer/forJobseeker*  (`method get`)<br><br>

3. Perusahaan publish lowongan dari daftar draft:<br>
*http://localhost:8888/project-mx100/api/job_offer/* (`method put`)<br>
**Params** : 
- CompanyId
- Tittle
- Description
- Status
- id<br><br>

4. Pencari kerja Apply pekerjaan:<br>
*http://localhost:8888/project-mx100/api/job_apply/* (`method post`)<br>
**Params** : 
- file (type *file*)
- JobOfferId
- JobseekerId<br><br>

5. Perusahaan melihat daftar pelamar pekerjaan:<br>
*http://localhost:8888/project-mx100/api/job_apply/* (`method get`)<br>
**Params** : 
- id (Opsional / spesifik data pelamar)

